<?php
header('Content-type: application/json');

$json = file_get_contents('php://input');
$jsonDecoded = json_decode($json);

$name = $jsonDecoded->text;
$uppercaseName = strtoupper($name);

$info = ["result" => $uppercaseName];
echo json_encode($info);



