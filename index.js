async function onButtonClick(){
    const input = document.getElementById("input-text").value;
    const inputObject = {text: input};
    const jsonRepresentation = JSON.stringify(inputObject);

    let reqHeader = new Headers();
    reqHeader.append('Content-Type', 'application/json');


    let initObject = {
        method: 'POST', headers: reqHeader, body: jsonRepresentation
    };

    const userRequest = new Request('./capitalizer.php', initObject);

    fetch(userRequest)
        .then(getJsonFromResponse)
        .then(changeResult)
        .catch(printError);
    

    /* DALSIA MOZNOST
    const response = await fetch(userRequest);
    const data = await getJsonFromResponse(response);
    changeResult(data);
    */

}


getJsonFromResponse = (response) =>{
    return response.json();
}


changeResult = (data) =>{
    document.getElementById("result").innerText = data.result;
}


printError = (err) =>{
    console.log("Something went wrong!", err);
}